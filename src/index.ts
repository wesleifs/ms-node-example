import express from "express";

const server = express();

server.get("/", (req, resp) => {
  resp.send("Hello world!!!");
});

server.listen(8080, () => {
  console.log("Server started on port 8080!!!");
});
